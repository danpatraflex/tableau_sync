const tableau = require("./connections/tableau.js");
const baan = require("./connections/baan.js");
const lavi = require("./connections/lavi.js");
const flw  = require("./connections/flw.js");

var job = function(data) {
    this.id = data['id'];
    this.queryString = data['get_data_query'].toString();
    this.dataSourceFrom = data['get_data_source'];
    this.dataSchemaTo = data['insert_data_schema'];
    this.dataTableTo = data['insert_data_table'];
    this.end_time = data['end_time'];
    this.is_running = data['is_running'];
    this.source = this.dataSchemaTo + '.' + this.dataTableTo;    
}

job.prototype.run = function(){  
    var that = this;
    var workConnection = flw;

    that.updateStatus(1);
    source = that.dataSchemaTo;
    table = that.dataTableTo;
    truncateQuery = "TRUNCATE " + source + "." + table;
    tableau.query(truncateQuery, function (error, rows, fields) {
        if (error){ throw error;}
    });

    switch(this.dataSourceFrom) {
        case 'lavi':
            lavi.JobObject(that);
            break;

        case 'baan':
            baan.JobObject(that);
            break;

        case 'flw':
        truncateQuery = "TRUNCATE " + source + "." + table
        tableau.query(truncateQuery, function (error, rows, fields) {
             if (error) throw error
        });

        workConnection.query(that.queryString, function (err, result, fields) {
            // console.log(result)
            if (err){throw new Error(err);}

            var count = result.length;
            console.log(that.source, ':',count,' rows')
            result.forEach(function(row) {
                var fields = [];
                for(field in row) {
                    fields.push(row[field]);
                }
                count--;
                if(count === 0) {
                    that.updateStatus(0);
                }
                insertQuery = "INSERT INTO " + that.source + " VALUES('" + fields.join("','") + "')";
                try{
                    tableau.query(insertQuery, function (error, rows, fields) {
                        });
                }catch{ 
                    that.updateStatus(0);
                    console.log(error);
                }
                
            })
        });
                break;
    }
};

job.prototype.updateStatus = function(value) {
    var time = new Date().getTime();
    sql = "UPDATE _scripts._scripts_jobs SET is_running = " + value + ", end_time = now() WHERE id = " + this.id;
    tableau.query(sql, function(err, result, fields) {
    });
}

module.exports = {
    Job: job
}