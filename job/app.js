const tableau = require("./connections/tableau.js");
const Job = require("./job.js").Job;
var cron = require('node-cron');

function getJobs(){
    tableau.query("SELECT * FROM _scripts._scripts_jobs", 
    function (error, result, fields) {
        if (error){ throw error; }

        Object.keys(result).forEach(function(key) {
            if(result[key].is_running==false && getIntervalFromLastJob(result[key].end_time) >= result[key].minutes_interval && result[key].is_active)  
            {
                var job = new Job(result[key]);
                job.run();
            }
        });
    });
  };

function getIntervalFromLastJob(end_time) {
    var time = new Date().getTime();
    var previos = new Date(end_time);
    var previos = previos.getTime();

    var timestamp = time - previos;
    return new Date(timestamp).getMinutes();
}

cron.schedule('* 7-19 * * *', () => {
    getJobs();
});  

cron.schedule('* 20 * * *', () => {
    sql = "UPDATE _scripts._scripts_jobs SET is_running = 0 ";
    tableau.query(sql, function(err, result, fields) {
    });
});
