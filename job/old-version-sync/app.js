const connection = require("./connection.js");
var cron = require("node-cron");

function getJobs(){
  tableauConnection.query("SELECT * FROM _scripts._scripts_jobs;", function (error, rows, fields) {
    if (error) throw error
      rows.forEach((row)=>{
          if(row.is_running==false && getIntervalFromLastJob(row.end_time) >= row.minutes_interval && row.is_active)
          updateTheJob(row)
      })
  })
}

function getData(row){
  sourceDB = row.get_data_source
  query = row.get_data_query
  tableauConnection.query("UPDATE _scripts._scripts_jobs SET is_running = '1' WHERE id = " + row.id, 
    function (error, rows, fields) {
      if (error) throw error
 });
  switch(sourceDB){
    
    case "flw": 
      flwConnection.query(query.toString(), function (error, rows, fields) {
      if (error) throw error
      console.log("job pulled data")
        insertData(row,rows,fields)
  })
  }
}

function insertData(jobRow,rowsToInsert,fields){
  source = jobRow.insert_data_schema
  table  = jobRow.insert_data_table
  truncateQuery = "TRUNCATE " + source + "." + table
  
  tableauConnection.query(truncateQuery, function (error, rows, fields) {
    if (error) throw error
    
 });

  rowsToInsert.forEach((row)=> {
    var str = '';
    var fields = [];

    for(field in row) {
      fields.push(row[field]);
    }  
    insertQuery = "INSERT INTO " + source + "." + table +" VALUES('" + fields.join("','") + "')";
    tableauConnection.query(insertQuery, function (error, rows, fields) {
      if (error) throw error
    }); 
  })

  tableauConnection.query("UPDATE _scripts._scripts_jobs SET is_running = '0' WHERE id = " + jobRow.id, function (error, rows, fields) {
   if (error) throw error
  });

  var time = new Date().getTime();
  tableauConnection.query("UPDATE _scripts._scripts_jobs SET end_time ="+ time +" WHERE id = " + jobRow.id, function (error, rows, fields) {
    if (error) throw error
  });
  console.log(jobRow.job_name + " - Inserting rows to table - Complete")
}

function getIntervalFromLastJob( end_time){
  var time = new Date().getTime();
  var timestamp = time - end_time
  return new Date(timestamp).getMinutes()
}

function updateTheJob(row){
  getData(row)
}

cron.schedule("*/1 * * * * ", () => {
});
getJobs()