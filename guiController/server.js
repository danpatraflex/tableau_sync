const connection = require("../connection.js");
//  Require Modules / Packages
const express = require('express');

//  Config
const config = {
    port: 4000
}

//  Data
//  TO DO: Create a Phonebook Module (external file) and use it in this API
const aContacts = [];
var nextId = 1;
aContacts.push( { id: nextId++, name: 'Alex', phone: '050' } );
aContacts.push( { id: nextId++, name: 'Dan', phone: '051' } );
aContacts.push( { id: nextId++, name: 'Amir', phone: '052' } );

//  Web Server Reference
const server = express();

//  Routes
server.get('/getContacts/', function(request, response){

    response.send( aContacts );

})

server.get('/getContact/:id', function(request, response){

    var contactId = request.params.id;
    //  Assume not found
    var result = {
        success: false,
        data: {}
    }

    for(contact of aContacts)
        if(contact.id == contactId) {
            //  Found - update result object accordingly
            result.success = true;
            result.data = contact;
            break;            
        }
    
    //  Answer
    response.send( result );
})


//  Start Server
console.log('Starting web server at port ' + config.port);
server.listen( config.port );
